# uncore DUF

The files in repository contains the experiments raw data for the work
presented in "DUF : Dynamic Uncore Frequency scaling to reduce power
consumption".

The experiments for this work were run on three platforms (nova,
chifflet and yeti from Grid'5000 :
https://www.grid5000.fr/w/Hardware).

The .csv files are organized in three directory, one per platform. The
powercap data are provided in the directory yeti/powercap. The
chifflet directory contains two files: sp-duf0 and sp-duf1 which show
the results with Nas Parallel Benchmarks for socket 0 (sp-duf0) and
socket 1 (sp-duf1) with duf_20.

The header of each file describes the presented data and each line is
the data for one application.